// import { createRouter, createWebHistory } from 'vue-router'
import Home from "../components/Home.vue";
import Post from "../components/Post.vue";
import NewPost from "../components/NewPost.vue";
import VueRouter from "vue-router";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/post/:id",
    name: "Post",
    component: Post,
  },
  {
    path: "/new/",
    name: "New post",
    component: NewPost,
  },
];

export const router = new VueRouter({
  routes, // `routes: routes`
});

export default router;
