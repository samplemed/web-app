function getRoute(route) {
  let authURL = new URL(process.env.VUE_APP_API_ROOT);

  return new URL(route, authURL).href;
}

async function authenticatedRequest(url, requestPayload) {
  const token = JSON.parse(window.localStorage.getItem("token"))["access"];

  let authHeader = { Authorization: `Bearer ${token}` };

  console.log(requestPayload);

  // if (typeof requestPayload.headers !== "undefined")
  requestPayload.headers = {
    ...requestPayload.headers,
    ...authHeader,
  };

  const res = await fetch(getRoute(url), requestPayload);

  return await res.json();
}

export async function getPosts() {
  const res = await fetch(getRoute("posts/"));

  return await res.json();
}

export async function getPost(id) {
  const res = await fetch(getRoute(`posts/${id}/`));

  return await res.json();
}

async function createCommenter(name, email) {
  const data = {
    name: name,
    email: email,
  };

  return fetch(getRoute("posts/commenters"), {
    headers: { "Content-type": "application/json" },
    method: "POST",
    body: JSON.stringify(data),
  }).then((res) => {
    return res.json();
  });
}

export async function createComment(
  postId,
  commenterName,
  commenterEmail,
  content,
  parentComment
) {
  const commenter = await createCommenter(commenterName, commenterEmail);

  const data = {
    post: postId,
    author: commenter.id,
    content: content,
    parent: parentComment,
  };

  const res = await fetch(getRoute("posts/comments/"), {
    method: "POST",
    headers: { "Content-type": "application/json" },
    body: JSON.stringify(data),
  });

  return await res.json();
}

export async function getComments(id) {
  const res = await fetch(getRoute(`posts/${id}/comments/`), {
    method: "GET",
    headers: { "Content-type": "application/json" },
  });

  return await res.json();
}

export async function getAuthenticatedUser() {
  return await authenticatedRequest("users/verify_credentials/", {});
}
export async function createPost(title, content, keywords) {
  const authedUser = await getAuthenticatedUser();

  console.log(authedUser);

  const data = {
    title: title,
    content: content,
    keywords: keywords,
    author: authedUser.id,
  };

  console.log({
    method: "POST",
    headers: { "Content-type": "application/json" },
    body: JSON.stringify(data),
  });

  return authenticatedRequest("posts/", {
    method: "POST",
    headers: { "Content-type": "application/json" },
    body: JSON.stringify(data),
  });
}
