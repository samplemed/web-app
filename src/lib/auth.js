export function auth(username, password) {
  let authURL = new URL(process.env.VUE_APP_API_ROOT);

  authURL = new URL("../token/", authURL);

  const data = { username: username, password: password };

  return fetch(authURL.href, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  }).then((r) => {
    if (r.ok) {
      return r.json();
    }
    throw new Error("Log in error");
  });
}

export function storeToken(token) {
  window.localStorage.setItem("token", JSON.stringify(token));
}

export function getToken() {
  return JSON.parse(window.localStorage.getItem("token"));
}

export function signOut() {
  window.localStorage.clear();
}
